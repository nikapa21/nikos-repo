# First System

A standalone Spring Boot Java 8 application that is parsing, storing locally and uploading to Adwords feeds(files) with
products targeted for online advertising.

## Initial phase

Each app instance is associated with a specific pair of MCC Adwords API key and clientCustomerId and during start up will
 attempt to retrieve the Adwords accounts and store them locally.

## Step 0. The feed_tags

We need to define at least one set of feed tags. This step is followed only when deploying the app for the first time
and it demands the manual insert of the following query in the mongo db.

## Step 1. Retrieving Accounts

The app will periodically try to retrieve and update the adwords Accounts of the MCC Adwords API key and the clientCustomerId.

## Step 2. Linking feeds and Parsing

The app is now able to parse csv and xml feed files containing records with categories/campaigns, product names/adgroups
and further details about each product (url, quantity, price, colour). For the parsing to take place we need to associate an
existing Account to a FeedFile (url). The app is periodically downloading the files associated to the retrieved accounts
and saves them as feed_products records based on the feed_tags configuration of the app.

## Step 3. The template configuration

Now that the feed_products records have been stored in the db it is time to create a template configuration that according
to which the creation of the creatives(campaigns, adgroups, ads, keywords) will take place in Adwords.

## Step 4. The scheduled upload of Creatives

The app will periodically upload create and update the Creatives per Adwords Account as they are defined by the template
 configuration linked to the Account.

# Setting up Locally

We first need to install java and mongo. Then we follow the steps 0-2 so that eventually we achieve 3.

## Java 8 installation

Install java 8. just download the .dmg file for mac os or use [brew](http://brew.sh/)

##Mongo installation

Install mongo (checked that it works with v2.4.9).http://docs.mongodb.org/manual/tutorial/install-mongodb-on-os-x/

    #While in ~/Downloads:
    curl -O http://downloads.mongodb.org/osx/mongodb-osx-x86_64-3.0.2.tgz
    #After download:
    tar -zxvf mongodb-osx-x86_64-3.0.2.tgz
    #Create the mongodb path:
    mkdir -p ~/Documents/mongodb
    #Copy the binary there:
    cp -R -n mongodb-osx-x86_64-3.0.2/ ~/Documents/mongodb
    #Now create the aliases for convenience
    nano ~/.bash_profile
    alias mongod=~/Documents/mongodb/bin/mongod
    alias mongo=~/Documents/mongodb/bin/mongo
    #close terminal app and restart it or reload the bash_profile
    mongo -version
    #should return the version 3 mongo.
    mongod
    #should get the daemon running

## Start the app

First make sure that you create the following directories with adequate rights/permissions for the user running the app

    mkdir /apps
    mkdir /apps/feed

In terminal (maybe using crontab), or in Intellij (for debug convenience)

    java -jar -Dspring.profiles.active=local target/feed-1.0-SNAPSHOT.jar

You can see the logs here

    tail -f /apps/feed/feed.log

## Step 0. Provide the default feed tags for the app.

For initializing the default feed tags of our app try the following db insert query:

    #open a browser
    http://127.0.0.1:8080/feed/template/feedtags/initialize
    #at the mongo console check that the feed_tags has one entry
    db.feed_tags.find()

## Step 1. Retrieving Accounts

The app is by default scheduled to retrieve Accounts once per day. When setting up locally we might want to trigger this
function more instantly so we just need to GET the forceRefresh endpoint

    curl 127.0.0.1:8080/feed/accounts/forceRefresh

You check the accounts inserted in mongo using the mongo client in terminal

    mongo
    #at the mongo console that appears try :
    db.accounts.findOne()
    #then count the inserted accounts:
    db.accounts.count()

## Step 2. Linking feeds and Parsing

At this point the app is properly initialized with Accounts and the FeedTags definitions of the Product Feed App.
Let's list the existing accounts with their feed files(nonexisting at the initial time)

    #open a browser tab
    http://127.0.0.1:8080/feed/accounts/

Create an association between a chosen account and a FeedFile through a POST request.
Pay attention to the parameters given, especially the url in this case is a URL to the local sample file

    curl -X POST "http://127.0.0.1:8080/feed/accounts/2809942940/download?filetype=csv&filename=feedFile.csv&delimeter=,&url=file:///Users/reven/feed/feedFile.csv&email=tasos.locality@gmail.com"
    #This should be the reply
    #{"id":null,"accountId":"2809942940","url":"file:///Users/reven/feed/feedFile.csv","path":null,"downloaded":false,"email":"tasos.locality@gmail.com","delimeter":",","quoteCharacter":"'","fileType":"csv","created":1458503256673,"downloadedDate":null,"parsed":false}

The feedfile is inserted for the specific accountid: 2809942940 with flag isDownloaded=false and isParsed=false

    db.accounts.find().pretty()
    #more db.accounts.findOne{"customerId":"2809942940"}

Wait (default 5 min) and let the FeedDownloaderService scheduler download the file (the flag isDownloaded becomes true)

    db.accounts.find().pretty()

Wait (default 5 min) and let the FeedParserService scheduler parse the accounts' feedfiles with isDownloaded is true.
The products/records will be parsed and inserted in feed_products.

    db.feed_products.count()

## Step 3. The template configuration

Now that the isDownloaded=true and isParsed=true we are ready to provide the template configuration for this feed.

    #open a browser tab
    http://127.0.0.1:8080/feed/accounts/2809942940/fetch?size=5

Note that the templateConfiguration is null

The user will start applying the tags to the returned columns in the GUI. after the user is done with the mappings
(configuring the template) he would want to apply the TemplateConfiguration for this AccountId.

    curl -X POST -d @/Users/reven/feed/feedTemplateConfigurationTest.json http://127.0.0.1:8080/feed/template/templateconfiguration  --header "Content-Type:application/json"

Now that the TemplateConfiguration is saved the next time that the Preview is fetched the template configuration will not be null. Try to fetch again this time for 3 size

    #open a browser tab
    http://127.0.0.1:8080/feed/accounts/2809942940/fetch?size=3

This describes the basic usage and user case scenario of the GUI part. Now the backend is responsible for combining the TemplateConfiguration on the parsed FeedProduct for this accountId and upload the outcomes accordingly to Adwords.

